import React from "react";
import SignIn from "./SignIn";
import Page2 from "./Page2";

type ContextProps = {
  authenticated: boolean
};

export const AppContext =
  React.createContext<Partial<ContextProps>>({});

const Header = () => {
  return <AppContext.Consumer>
    {
      ({ authenticated }) => {
        if (authenticated) {
          return (
            <div style={{ textAlign: "center" }}>
              <Page2
              />
            </div>
          );
        }
        return (
          <div style={{ textAlign: "center" }}>
            <SignIn
            />
          </div>
        );
      }
    }
  </AppContext.Consumer>
}

const App = () => {
  return <AppContext.Provider value={{
    authenticated: false,
  }}>
    <Header />
  </AppContext.Provider>
}

export default App;