import * as React from 'react';
import App from "./App";
import { DataGrid } from '@mui/x-data-grid';
import { ThemeProvider } from '@emotion/react';
import { Box, Container, createTheme } from '@mui/material';

const theme = createTheme({
    palette: {
        primary: {
            main: '#9c27b0',
        },
        background: {
            default: "#DDDDDE",
        }
    }
});

const columns = [
    {field: 'id', headerName: 'ID', width: 150},
    {
        field: 'imageName',
        headerName: 'Image Name',
        width: 150,
        editable: true,
    },
    {
        field: 'size',
        headerName: 'Size',
        width: 150,
        editable: true,
    },
    {
        field: 'recognitionResult',
        headerName: 'Recognition Result',
        type: 'number',
        width: 200,
        editable: true,
    },
    {
        field: 'imgDownLnk',
        headerName: 'Image Download Link',
        width: 220,
        editable: true,
    },
];

const rows = [
    { id: 1, imageName: 'image1.jpg', size: '1MB', recognitionResult: 'Jon', imgDownLnk: 35 },
    { id: 2, imageName: 'image2.jpg', size: '1.1MB', recognitionResult: 'Cersei', imgDownLnk: 42 },
    { id: 3, imageName: 'image3.jpg', size: '1.2MB', recognitionResult: 'Jaime', imgDownLnk: 45 },
    { id: 4, imageName: 'image4.jpg', size: '1MB', recognitionResult: 'Arya', imgDownLnk: 16 },
    { id: 5, imageName: 'image5.jpg', size: '1MB', recognitionResult: 'Daenerys', imgDownLnk: null },
    { id: 6, imageName: 'image6.jpg', size: '1MB', recognitionResult: null, imgDownLnk: 150 },
    { id: 7, imageName: 'image7.jpg', size: '1MB', recognitionResult: 'Ferrara', imgDownLnk: 44 },
    { id: 8, imageName: 'image8.jpg', size: '1MB', recognitionResult: 'Rossini', imgDownLnk: 36 },
    { id: 9, imageName: 'image9.jpg', size: '1MB', recognitionResult: 'Harvey', imgDownLnk: 65 },
];

export default function TablePage() {
    return (
            <Container component="main">
                    <div style={{ height: 400, width: '100%', marginTop: '250px'}}>
                        <DataGrid
                            rows={rows}
                            columns={columns}
                            pageSize={5}
                            rowsPerPageOptions={[5]}
                            checkboxSelection
                            disableSelectionOnClick
                        />
                    </div>
            </Container>
    );
}