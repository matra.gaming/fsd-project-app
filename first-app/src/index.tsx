import ReactDOM from 'react-dom'
import App from './App'
import SignIn from "./SignIn";
import Page2 from "./Page2";
import reportWebVitals from './reportWebVitals';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import Page3 from './Page3';
import DatePicker from './DatePicker';

const routing = (
    <Router>
        <div className="page">
            <Switch>
                <Route exact path="/" component={App} />
                <Route exact path='/signin' component={SignIn} />
                <Route exact path='/page2' component={Page2} />
                <Route exact path='/page3' component={Page3} />
                <Route exact path='/DatePicker' component={DatePicker} />
            </Switch>
        </div>
    </Router>
)
ReactDOM.render(routing, document.getElementById('root'))

reportWebVitals();