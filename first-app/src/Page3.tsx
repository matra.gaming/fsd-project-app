import { Button, Grid, Typography } from '@mui/material';
import { useEffect, useState } from 'react';
import axios from "axios";

function ImagePage() {

    const [inputFile, setInputFile] = useState<HTMLInputElement | null>(null);
    useEffect(() => {
        setInputFile(document.getElementById("imgupload") as HTMLInputElement);
    }, []);

    const baseURL = "http://localhost:3001/evaluate";

    const axiosInstance = axios.create({
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers": "X-Requested-With"
        }
    });

    const [file, setFile] = useState("");

    const handleUpload = () => {
        inputFile?.click();
    };

    function handleChange(e: any) {
        if (e.target.files.length !== 0) {
            let url = URL.createObjectURL(e.target.files[0]);
            setFile(url);
            e.preventDefault()
            let formData = new FormData();
            formData.append('images', e.target.files[0]);
            axiosInstance.post(baseURL, formData, {
            }).then((response: any) => {
                console.log((response.data))
            })
            console.log(url);
        }
    }

    return (
        <Grid sx={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            bgcolor: 'whitesmoke',
            minHeight: '98vh',
            minWidth: '100vh'
        }}>
            <div style={{ width: '80%', marginTop: '50px' }}>
                <Typography align='right'>
                    <input type="file" id="imgupload" style={{ display: 'none' }} onChange={handleChange} />
                    <Button variant="contained" style={{ maxWidth: '200px', maxHeight: '50px', minWidth: '200px', minHeight: '50px' }} onClick={handleUpload}>Upload</Button>
                </Typography>
            </div>
            <div>
                <img src={file}></img>
            </div>
        </Grid>
    )
}

export default ImagePage;