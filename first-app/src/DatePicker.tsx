import { MenuItem, Select, SelectChangeEvent } from '@mui/material';
import React, { useEffect, useState } from 'react';
import './DatePicker.scss';
import { format } from "date-fns";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import moment from 'moment';
import axios from 'axios';


function DatePicker() {

    const gcpInstance = axios.create({
        baseURL: "https://us-central1-fsd-challenge.cloudfunctions.net/",
        timeout: 10000
    });

    const [state, setState] = useState({
        startDate: new Date(),
        endDate: new Date()
    });
    const [currentMonth, setCurrentMonth] = useState('10');
    const [selectedYear, setYear] = useState('2021');
    const [showDatepicker, setShowDatepicker] = useState(false);
    const [rows, setRows] = useState([{ "start_date": 'nothing here', "end_date": 'nothing here' }]);

    const monthDays = {
        "January": ['', '', '', '', '', '', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, '', '', '', '', ''],
        "February": ['', '', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, '', '', '', '', '', '', '', '', '', '', '', ''],
        "March": ['', '', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, '', '', '', '', '', '', '', '', ''],
        "April": ['', '', '', '', '', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, '', '', '', '', '', '', ''],
        "May": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, '', '', '', '', '', '', '', '', '', '', ''],
        "June": ['', '', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, '', '', '', '', '', '', '', '', '', ''],
        "July": ['', '', '', '', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, '', '', '', '', '', '', ''],
        "August": ['', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, '', '', '', '', '', '', '', '', '', ''],
        "September": ['', '', '', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, '', '', '', '', '', '', '', '', ''],
        "October": ['', '', '', '', '', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, '', '', '', '', '', ''],
        "November": ['', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, '', '', '', '', '', '', '', '', '', '', ''],
        "December": ['', '', '', 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, '', '', '', '', '', '', '', '']
    }

    const years = [2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030]
    function handleChangeSelect(e: SelectChangeEvent<number>) {
        setCurrentMonth(e.target.value.toString())
    }

    var firstDate = new Date();
    var secondDate = new Date();
    var oldDate = new Date();

    function handleChangeYear(e: SelectChangeEvent<number>) {
        setYear((years[(e.target.value) as unknown as number]).toString())
    }

    function changeBackgroundHover(e: any) {
        e.target.style.background = 'grey';
    }

    function changeBackgroundLeave(e: any) {
        e.target.style.background = 'lightskyblue';
    }

    function currentDateBackground(e: any) {
        const today = 31;
        if (e === today) {
            e.target.style.background = 'red';
        }
    }

    function getDaysForMonth(selectValue: String) {
        switch (selectValue) {
            case "1": {
                return monthDays.January;
            }
            case "2": {
                return monthDays.February;
            }
            case "3": {
                return monthDays.March;
            }
            case "4": {
                return monthDays.April;
            }
            case "5": {
                return monthDays.May;
            }
            case "6": {
                return monthDays.June;
            }
            case "7": {
                return monthDays.July;
            }
            case "8": {
                return monthDays.August;
            }
            case "9": {
                return monthDays.September;
            }
            case "10": {
                return monthDays.October;
            }
            case "11": {
                return monthDays.November;
            }
            case "12": {
                return monthDays.December;
            }
            default: {
                return [];
            }
        }
    }

    function handleDates(startDate: Date, endDate: Date) {
        let scheduleToBeAdded = {
            "start_date": moment(startDate).format("YYYY-MM-DD"),
            "end_date": moment(endDate).format("YYYY-MM-DD")
        };

        setTimeout(function () { setState({ startDate: startDate, endDate: endDate }); }, 100);
        setShowDatepicker(false);

        let newRows = rows;
        newRows.push(scheduleToBeAdded);
        setRows((oldRows: any) => [...oldRows, newRows]);
        //now insert into db and if error occurs then delete the last row
        gcpInstance.post("/postSchedule", scheduleToBeAdded)
            .then((response) => {
                if (response.status !== 200) {
                    const filteredSchedules = rows.filter(item => item !== scheduleToBeAdded);
                    setTimeout(function() { alert(response); }, 100);
                    setRows(filteredSchedules);
                } else {
                    console.log(response);
                }
            }).catch((err) => {
                console.log(err.message);
                const filteredSchedules = rows.filter(item => item !== scheduleToBeAdded);
                setRows(filteredSchedules);
                setTimeout(function() { alert(err.message); }, 100);
            });
    }

    const setDateValue = (date: string | number, currentMonth: string) => () => {

        let same = firstDate.getTime() === oldDate.getTime();
        if (same === true) {
            firstDate = new Date(
                parseInt(selectedYear),
                parseInt(currentMonth) - 1,
                parseInt(date.toString())
            );
            console.log(firstDate);
        } else if (same === false) {
            secondDate = new Date(
                parseInt(selectedYear),
                parseInt(currentMonth) - 1,
                parseInt(date.toString())
            );
            handleDates(firstDate, secondDate);
        }
    };

    const toggleDatepicker = () => setShowDatepicker((prev) => !prev);

    async function getDataFromDb() {
        let results: { id: number, start_date: string, end_date: string }[] = [];
        await gcpInstance.get("getSchedules").then((res: any) => {
            res.data.forEach(function (dbDate: any) {
                results.push({ "id": dbDate.id, "start_date": dbDate.start_date, "end_date": dbDate.end_date });
            });
            setRows(results);
        });
    }

    useEffect(() => {
        getDataFromDb();
    }, []);

    return (
        <><div className='dateTable'>
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">Start Date</TableCell>
                            <TableCell align="left">End Date</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((row) => (
                            <TableRow>
                                <TableCell align="left">{row.start_date}</TableCell>
                                <TableCell align="left">{row.end_date}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
            <div className="container">
                <div>
                    <strong>Start date: </strong>
                    <input
                        type="text"
                        readOnly

                        className="cursor-pointer w-full pl-4 pr-10 py-3 leading-none rounded-lg shadow-sm focus:outline-none focus:shadow-outline text-gray-600 font-medium"
                        placeholder="Select date"
                        value={format(state.startDate, "yyyy-MM-dd")}
                        onClick={toggleDatepicker} />
                </div>
                <div>
                    <strong>End date: </strong>
                    <input
                        type="text"
                        readOnly

                        className="cursor-pointer w-full pl-4 pr-10 py-3 leading-none rounded-lg shadow-sm focus:outline-none focus:shadow-outline text-gray-600 font-medium"
                        placeholder="Select date"
                        value={format(state.endDate, "yyyy-MM-dd")}
                        onClick={toggleDatepicker} />
                </div>
                {showDatepicker && (
                    <div className="calendar-container">
                        <Select
                            className="monthPicker"
                            defaultValue={10}
                            onChange={handleChangeSelect}>
                            <MenuItem value={1}>January</MenuItem>
                            <MenuItem value={2}>February</MenuItem>
                            <MenuItem value={3}>March</MenuItem>
                            <MenuItem value={4}>April</MenuItem>
                            <MenuItem value={5}>May</MenuItem>
                            <MenuItem value={6}>June</MenuItem>
                            <MenuItem value={7}>July</MenuItem>
                            <MenuItem value={8}>August</MenuItem>
                            <MenuItem value={9}>September</MenuItem>
                            <MenuItem value={10}>October</MenuItem>
                            <MenuItem value={11}>November</MenuItem>
                            <MenuItem value={12}>December</MenuItem>
                        </Select>
                        <Select onChange={handleChangeYear} className="yearPicker" defaultValue={21}>
                            {years.map((year, index) => (
                                <MenuItem value={index} key={index}>{year}</MenuItem>
                            ))}
                        </Select>
                        <div className="child">S</div>
                        <div className="child">M</div>
                        <div className="child">T</div>
                        <div className="child">W</div>
                        <div className="child">T</div>
                        <div className="child">F</div>
                        <div className="child">S</div>
                        {getDaysForMonth(currentMonth).map(day => (
                            <div className="child" onClick={setDateValue(day, currentMonth)} onMouseMove={changeBackgroundHover} onMouseLeave={changeBackgroundLeave} onLoad={currentDateBackground}>{day}</div>
                        ))}
                    </div>
                )}
            </div></>
    )
}

export default DatePicker;